#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "listBasic.h"
#include <iostream>

TEST_CASE("List basics", "[list]")
{
  List<int,10> myList;
  myList.add(1);
  myList.add(2);
  myList.add(3);
  REQUIRE(myList.get(0)==1);
  REQUIRE(myList.get(1)==2);
  REQUIRE(myList.get(2)==3);
  std::cout<<myList<<std::endl;
}

TEST_CASE("List remove", "[list]")
{
  List<int,10> myList;
  myList.add(1);
  myList.add(2);
  myList.add(3);
  myList.remove(1);
  REQUIRE(myList.get(0)==1);
  REQUIRE(myList.get(1)==3);
  std::cout<<myList<<std::endl;
}
