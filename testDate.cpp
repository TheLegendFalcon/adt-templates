#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "date.h"

TEST_CASE("Date basic", "[date]")
{
  Date birthday = Date(5,12,1998);
  std::cout << birthday;
  REQUIRE(birthday.getDay()==5);
  REQUIRE(birthday.getMonth()==12);
  REQUIRE(birthday.getYear()==1998);
}

TEST_CASE("Date Comparison","[date]")
{
  Date birthdayOri = Date(5,12,1998);
  Date birthdayEq = Date(5,12,1998);
  Date birthdayY = Date(5,12,1988);  
  Date birthdayM = Date(5,11,1998);
  Date birthdayD = Date(4,12,1998);
  
  REQUIRE(!(birthdayEq < birthdayOri));
  REQUIRE(birthdayY < birthdayOri);
  REQUIRE(birthdayM < birthdayOri);
  REQUIRE(birthdayD < birthdayOri);

}
