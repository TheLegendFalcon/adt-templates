#include "date.h"
#include <iostream>
#include <ostream>
Date::Date(int day, int month, int year)
{
  this->day=day;
  this->month=month;
  this->year=year;
}

int Date::getDay()
{
  return this->day;
}

int Date::getMonth()
{
  return this->month;
}
int Date::getYear()
{
  return this->year;
}

std::ostream& operator<<(std::ostream& out, Date date)
{
  out << date.day <<"/" << date.month << "/" << date.year << std::endl;
  return out;
}

bool operator <(const Date& lhs, const Date& rhs)
{
  if(lhs.year < rhs.year){
    return true;
  }
  else if((lhs.year == rhs.year)
	  && (lhs.month < rhs.month)){
    return true;
  }
  else if((lhs.year == rhs.year)
	  && (lhs.month == rhs.month)
	  && (lhs.day < rhs.day)){
    return true;
  }
  else{
    return false;
  }

}
