#ifndef _LIST_H_
#define _LIST_H_

#include <iostream>
#include <ostream>

template <typename T, int maxSize>
class List{

private:
  T *data;
  int size;
public:
  //Templates are ugly and they force us to define the methods inside the .h in order to avoid linking problems...
  List()
  {
    this->data = new T[maxSize];
    this->size = 0;
  }

  void add(T item)
  {
    this->data[this->size++]=item;
  }

  T get(int pos)
  {
    return this->data[pos];
  }

  void remove(int pos)
  {
    for(int i=pos; i < this->size-1; i++)
      {
	this->data[i] = this->data[i+1];
      }
    this->size--;
  }

  int getSize()
  {
    return this->size;
  }

  friend std::ostream& operator <<(std::ostream& out, List<T,maxSize> mylist)
  {
    out << "[";
    for(int i = 0; i< mylist.size; i++)
      {
	out << mylist.data[i];
	if(i+1 < mylist.size)
	  out << ",";
      }
    out << "]";
    return out;
  }
};
#endif
